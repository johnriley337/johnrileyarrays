#include <iostream>
#include <conio.h>
#include <string>
#include <vector>

using namespace std;

struct Employee {
	int ID;
	string FirstName;
	string LastName;
	double Payrate;
	int Hours;
};

int main() 
{
	Employee data;
	vector<Employee> person;

	for (int i = 0; i < 5; i++) {
		cout << "\n" << "INPUT";
		cout << "\n" << "ID: ";
		cin >> data.ID;

		cout << "First Name: ";
		cin >> data.FirstName;

		cout << "Last Name: ";
		cin >> data.LastName;

		cout << "Pay Rate: ";
		cin >> data.Payrate;

		cout << "Hours: ";
		cin >> data.Hours;

		int GrossPay = data.Payrate * data.Hours;

		cout << "\n" << "OUTPUT";
		cout << "\n" << "ID: " << data.ID << "\n";
		cout << "First Name: " << data.FirstName << "\n";
		cout << "Last Name: " << data.LastName << "\n";
		cout << "Gross Pay: " << GrossPay << "\n";

		person.push_back(data);
	}

	_getch();
	return 0;
}